package Core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

public class LogIn {
	// action="http://phc.prontonetworks.com/cgi-bin/authlogin?URI=http://www.msftncsi.com/redirect"
	// method="POST"
	// name="userId" type="text"
	// name="password" type="password"
	// type="hidden" name="serviceName" value="ProntoAuthentication"
	// type="submit" name="Submit22" value="Login"
	String charset = java.nio.charset.StandardCharsets.ISO_8859_1.toString();
	URL url;
	URLConnection con;
	PrintWriter wr;
	StringBuilder parameters;
	String dynAddr;
	String userId;
	String password;
	String htmlResp;

	String connect(String userId, String password) {
		try {
			this.userId = userId;
			this.password = password;
			url = new URL("http://phc.prontonetworks.com/cgi-bin/authlogin?"); // URI=http://www.msftncsi.com/redirect
			con = url.openConnection();
			con.setConnectTimeout(60 * 1000);
			con.setDoOutput(true);
			wr = new PrintWriter(con.getOutputStream(), true);
			parameters = new StringBuilder();
			parameters.append("userId=" + URLEncoder.encode(userId, charset));
			parameters.append("&");
			parameters.append("password=" + URLEncoder.encode(password, charset));
			parameters.append("&");
			parameters.append("serviceName=" + URLEncoder.encode("ProntoAuthentication", charset));
			parameters.append("&");
			parameters.append("Submit22=" + URLEncoder.encode("Login", charset));
			wr.println(parameters);
			wr.close();
			BufferedReader br = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
			String line = br.readLine();
			br.close();
			this.htmlResp = line;
			return line;
		} catch (Exception e) {
			System.out.println(e);
			return "Error";
		}
	}

	void logOut() {
		HttpURLConnection con;
		try {
			url = new URL("http://phc.prontonetworks.com/cgi-bin/authlogout");
			con = (HttpURLConnection) url.openConnection();
			//System.out.print(con.getContent());
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	void getUsage(String month, String day, String year, String mEnd,
	String dEnd, String yEnd) {
		try {
			Document doc = Jsoup.parse(htmlResp);
			Elements dyn = doc.select("a.orangeText10:nth-child(2)");
			dynAddr = dyn.first().attr("href");
			//System.out.println(dynAddr);
			String usgFormUrl = getUsgFormUrl(dynAddr);
			//System.out.println(usgFormUrl);
			doc = Jsoup.parse(getUsgHtml(usgFormUrl, month, day, year, mEnd,
			dEnd, yEnd));
			Elements usg = doc.select(".subTextRight:nth-child(5) b");
			System.out.println(usg);

		} catch (Exception e) {
			System.out.print(e);
		}
	}

	String getUsgFormUrl(String dynAddr) throws Exception {
		String html = Jsoup.connect(dynAddr).timeout(60 * 1000).get()
			.select("form").first().attr("action");
		return dynAddr.substring(0, 20) + html;
	}

	String getUsgHtml(String usgFormUrl, String month, String day, String year, String mEnd,
	String dEnd, String yEnd) throws Exception {
		String charset = java.nio.charset.StandardCharsets.UTF_8.toString();
		URL url = new URL(usgFormUrl);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(120 * 1000);
		//System.out.println("1");
		con.setDoOutput(true);
		//System.out.println("2");
		PrintWriter wr = new PrintWriter(con.getOutputStream(), true);
		//System.out.println("3");
		StringBuilder parameters = new StringBuilder();
		parameters.append("location=" + URLEncoder.encode("VIT-Vellore", charset));
		parameters.append("&");
		parameters.append("parameter=" + URLEncoder.encode("custom", charset));
		parameters.append("&");
		parameters.append("customStartMonth=" + URLEncoder.encode(month, charset));
		parameters.append("&");
		parameters.append("customStartDay=" + URLEncoder.encode(day, charset));
		parameters.append("&");
		parameters.append("customStartYear=" + URLEncoder.encode(year, charset));
		parameters.append("&");
		parameters.append("customEndMonth=" + URLEncoder.encode(mEnd, charset));
		parameters.append("&");
		parameters.append("customEndDay=" + URLEncoder.encode(dEnd, charset));
		parameters.append("&");
		parameters.append("customEndYear=" + URLEncoder.encode(yEnd, charset));
		parameters.append("&");
		parameters.append("button=" + URLEncoder.encode("View", charset));
		//System.out.println(parameters);
		wr.println(parameters);
		//System.out.println("4");
		wr.close();
		String inputLine;
		StringBuffer response = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(
		con.getInputStream()));
		while ((inputLine = br.readLine()) != null) {
			response.append(inputLine);
		}
		//System.out.println(response);
		br.close();
		return response.toString();
	}
}